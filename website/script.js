function decodeAddressAndSendMail(emailBase64) {
    var mail = atob(emailBase64);
    location.href = `mailto:${mail}`;
}

function isStaging() {
    var isStaging = location.hostname.startsWith('staging');
    if (isStaging) {
        var body = document.body;
        var alert = document.createElement('div');
        alert.id = 'alert';
        alert.innerText = 'Dies ist eine Test-Website.';
        body.prepend(alert);
    }
}
